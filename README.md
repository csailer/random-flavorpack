[![coverage report](https://gitlab.com/serial-lab/random-flavorpack/badges/master/coverage.svg?job=python3_5_unit_test)](https://gitlab.com/serial-lab/random-flavorpack/commits/master) [![build status](https://gitlab.com/serial-lab/random-flavorpack/badges/master/build.svg)](https://gitlab.com/serial-lab/random-flavorpack/commits/master)

# Random FlavorPack
The random-flavorpack provides efficient, non-repeating random number generation and a corresponding Randomized Region API to SerialBox.
For more information on the Random FlavorPack see the documentation here:

[Random FlavorPack Documentation](https://serial-lab.gitlab.io/random-flavorpack/)


![random](logo.png)
